﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstellaBank.Model
{
    public class CategoryModel
    {
        /// <summary>
        /// Gets or sets the category model identifier.
        /// </summary>
        /// <value>
        /// The category model identifier.
        /// </value>
        public int CategoryModelID { get; set; }
        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        /// <value>
        /// The name of the category.
        /// </value>
        public string CategoryName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryModel"/> class.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        public CategoryModel(string categoryName)
        {
            CategoryName = categoryName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryModel"/> class.
        /// </summary>
        public CategoryModel()
        {
        }
    }
}
