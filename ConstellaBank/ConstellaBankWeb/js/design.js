$(document).ready(function () {
    $('.modal-trigger').leanModal();
    $('select').material_select();
    $(".button-collapse").sideNav();
    moment.locale('fr');
    $("#account_look").html('Situation de votre compte au ' + moment().format("LL") + ' :');
    $(".button-collapse").sideNav({ closeOnClick: true })
});