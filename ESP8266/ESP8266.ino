#include <MQTT.h>
#include <PubSubClient.h>

#include <Constellation.h>
#include <Adafruit_NeoPixel.h>
#include <math.h>

#define NUM_PIXELS 24
const int maxBalance = 3000;
float currentBalance = 0;

Adafruit_NeoPixel pixels(NUM_PIXELS, D2, NEO_GRB | NEO_KHZ800);

// ESP8266 Wifi
#include <ESP8266WiFi.h>

char ssid[] = "xxx";
char password[] = "xxx";

//Server Homekit
IPAddress server(192, 168, xxx, xxx);

// Constellation client
Constellation<WiFiClient> constellation("192.168.xxx.xxx", 8088, "BankSentinel", "LedBank", "xxx");

void callback(const MQTT::Publish& pub) {
  // handle message arrived
  Serial.print(pub.topic());
  Serial.print(" => ");

  Serial.println(pub.payload_string());

  if (pub.payload_string() == "on")
  {
    constellation.sendMessageWithSaga([](JsonObject & json) {
      //json.prettyPrintTo(Serial);
      gauge(json["Data"]);
    }, Package, "ESPBrain", "getCurrentBalanceForConnected", "{}");
  }
  else
  {
    switchAllOff();
  }

}

//Client pki
WiFiClient wclient;
PubSubClient client(wclient, server);

void setup(void) {
  Serial.begin(115200);  delay(10);
  pixels.begin();
  switchAllOff();
  //setupCircle();
  // Set Wifi mode
  if (WiFi.getMode() != WIFI_STA) {
    WiFi.mode(WIFI_STA);
    delay(10);
  }

  // Connecting to Wifi
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected. IP: ");
  Serial.println(WiFi.localIP());

  // Get package settings (see BasicDemo example)

  // Register MessageCallbacks (see MessageCallbackDemo example)

  constellation.registerMessageCallback("SwitchOnConstellabank", MessageCallbackDescriptor().setDescription("Switch LED for Constellabank"), [](JsonObject & json) {
    constellation.sendMessageWithSaga([](JsonObject & json) {
      //json.prettyPrintTo(Serial);
      gauge(json["Data"]);
    }, Package, "ESPBrain", "getCurrentBalanceForConnected", "{}");
  });

  constellation.registerMessageCallback("SwitchAllOff", MessageCallbackDescriptor().setDescription("Switch all LED off"), [](JsonObject & json) {
    switchAllOff();
  });

  constellation.registerMessageCallback("DebitAdded", MessageCallbackDescriptor().setDescription("New immediate debit added"), [](JsonObject & json) {
    int i = 3;
    while (i != 0) {
      switchAllOn(pixels.Color(100, 0, 0));
      delay(500);
      switchAllOff();
      delay(500);
      i = i - 1;
    }
  });

  constellation.registerMessageCallback("CreditAdded", MessageCallbackDescriptor().setDescription("New immediate credit added"), [](JsonObject & json) {
    int i = 3;
    while (i != 0) {
      switchAllOn(pixels.Color(0, 100, 0));
      delay(500);
      switchAllOff();
      delay(500);
      i = i - 1;
    }
  });

  constellation.registerMessageCallback("CreditFutureAdded", MessageCallbackDescriptor().setDescription("New future credit added"), [](JsonObject & json) {
    int i = 2;
    while (i != 0) {
      circleLights(pixels.Color(0, 100, 0));
      i = i - 1;
    }
  });

  constellation.registerMessageCallback("DebitFutureAdded", MessageCallbackDescriptor().setDescription("New future debit added"), [](JsonObject & json) {
    int i = 2;
    while (i != 0) {
      circleLights(pixels.Color(100, 0, 0));
      i = i - 1;
    }
  });

  constellation.registerMessageCallback("SetGauge", MessageCallbackDescriptor().setDescription("Set Gauge").addParameter("Balance", "System.Double"), [](JsonObject & json) {
    float balance = (float)json["Data"];
    //Serial.print(balance);
    currentBalance = balance;
    gauge(balance);
  });

  // Declare the package descriptor
  constellation.declarePackageDescriptor();

  //Set callback for MQTT client
  client.set_callback(callback);

  if (!client.connected()) {
    if (client.connect("ESP8266: AdyLight")) {
      //client.publish("outTopic", "hello world");
      client.subscribe("Constellabank");
      Serial.println("Subscribed");
    }
  }

  // WriteLog info
  constellation.writeInfo("Virtual Package on '%s' is started !", constellation.getSentinelName());
}

void switchAllOn(uint32_t color) {
  for (int i = 0; i < NUM_PIXELS; i++)
  {
    pixels.setPixelColor(i, color);
    pixels.show();
    delay(0);
  }
}

void setupCircle(){
  for (int i = 0; i < NUM_PIXELS; i++)
  {
    pixels.setPixelColor(i, pixels.Color(100, 50, 0));
    pixels.show();
    delay(100);
  }
}

void circleLights(uint32_t color) {
  for (int i = 0; i < NUM_PIXELS; i++)
  {
    pixels.setPixelColor(i, color);
    pixels.show();
    delay(30);
  }
  for (int i = 0; i < NUM_PIXELS; i++)
  {
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
    pixels.show();
    delay(30);
  }
}

void switchAllOff() {
  for (int i = 0; i < NUM_PIXELS; i++)
  {
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
    pixels.show();
    delay(0);
  }
  //Serial.println("All OFF");
}

void setLight(int beginLed, int endLed, uint32_t color) {
  for (int i = beginLed; i < endLed; i++)
  {
    pixels.setPixelColor(i, color);
    pixels.show();
    delay(0);
  }
}

void gauge(float balance) {
  //Produit en croix
  //NUM_PIXELS = maxBalance
  //numberLedToSwitch = balance
  //float test = (float) (NUM_PIXELS * balance) / maxBalance;
  //Serial.print(test);
  int numberLedToSwitch = ceil( (float) (NUM_PIXELS * balance) / maxBalance);
  //constellation.writeInfo("Number of LED to switch On : %d", numberLedToSwitch);

  switchAllOff();
  if (balance <= 0) {
    setLight(0, NUM_PIXELS, pixels.Color(100, 0, 0));
  } else if (numberLedToSwitch <= NUM_PIXELS / 3) {
    setLight(0, numberLedToSwitch, pixels.Color(100, 0, 0));
  } else if (numberLedToSwitch <= 2 * NUM_PIXELS / 3) {
    setLight(0, numberLedToSwitch, pixels.Color(100, 30, 0));
  } else if (numberLedToSwitch <= 3 * NUM_PIXELS / 3) {
    setLight(0, numberLedToSwitch, pixels.Color(0, 100, 0));
  } else if (numberLedToSwitch > 3 * NUM_PIXELS / 3) {
    setLight(0, NUM_PIXELS, pixels.Color(0, 100, 0));
  }
}

void loop(void) {
  constellation.loop();
  if (WiFi.status() == WL_CONNECTED) {
    if (client.connected())
      client.loop();
  }
}
