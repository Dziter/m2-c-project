﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstellaBank.Model;
using Constellation.Package;
using LiteDB;

namespace ConstellaBank.StateObjectModel
{
    [StateObject]
    public class PersonModel
    {
        /// <summary>
        /// Gets or sets the person model identifier.
        /// </summary>
        /// <value>
        /// The person model identifier.
        /// </value>
        public int PersonModelId { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the name of the login.
        /// </summary>
        /// <value>
        /// The name of the login.
        /// </value>
        [BsonIndex(unique:true)]
        public string LoginName { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }
        /// <summary>
        /// Gets or sets the user settings.
        /// </summary>
        /// <value>
        /// The user settings.
        /// </value>
        public UserSettingModel UserSettings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonModel"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="loginName">Name of the login.</param>
        /// <param name="password">The password.</param>
        public PersonModel(string name, string loginName, string password)
        {
            Name = name;
            LoginName = loginName;
            Password = password;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonModel"/> class.
        /// </summary>
        public PersonModel()
        {
        }
    }


}
