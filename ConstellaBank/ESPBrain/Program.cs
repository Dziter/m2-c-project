﻿using Constellation;
using Constellation.Package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace ESPBrain
{
    public class Program : PackageBase
    {
        [StateObjectLink("*","*","*", "ConstellaBank.Model.AccountStateObject")]
        public StateObjectNotifier AccountStateObject { get; set; }

        [StateObjectLink("*", "*", "CurrentConnection")]
        public StateObjectNotifier LoginStateObject { get; set; }

        /// <summary>
        /// Gets or sets the current account connected.
        /// </summary>
        /// <value>
        /// The current account connected.
        /// </value>
        public int currentAccountConnected { get; set; } = -1;
        /// <summary>
        /// Gets or sets the current balance.
        /// </summary>
        /// <value>
        /// The current balance.
        /// </value>
        public double currentBalance { get; set; } = 0;

        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning, PackageHost.IsConnected);

            AccountStateObject.ValueChanged += AccountStateObject_ValueChanged;
            LoginStateObject.ValueChanged += LoginStateObject_ValueChanged;
        }

        /// <summary>
        /// Handles the ValueChanged event of the LoginStateObject control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="StateObjectChangedEventArgs"/> instance containing the event data.</param>
        private void LoginStateObject_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            PackageHost.WriteInfo("Login");
            currentAccountConnected = LoginStateObject.DynamicValue.ID;
            currentBalance = LoginStateObject.DynamicValue.BalanceOnLogin;
        }

        /// <summary>
        /// Handles the ValueChanged event of the AccountStateObject control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="StateObjectChangedEventArgs"/> instance containing the event data.</param>
        private void AccountStateObject_ValueChanged(object sender, StateObjectChangedEventArgs e)
        {
            PackageHost.WriteInfo("Account");
            if (e.NewState.DynamicValue.ID == currentAccountConnected)
            {
                object outValue;
                if (e.NewState.Metadatas.TryGetValue("lastOperationType", out outValue))
                {
                    string lastOpeType = (string)outValue;
                    currentBalance = e.NewState.DynamicValue.Balance;
                    if (lastOpeType == "ImmediatDebit")
                    {
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "DebitAdded", null);
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "SetGauge", currentBalance);
                    }
                    else if (lastOpeType == "ImmediatCredit")
                    {
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "CreditAdded", null);
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "SetGauge", currentBalance);
                    }
                    else if (lastOpeType == "FutureDebit")
                    {
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "DebitFutureAdded", null);
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "SetGauge", currentBalance);
                    }
                    else if (lastOpeType == "FutureCredit")
                    {
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "CreditFutureAdded", null);
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "SetGauge", currentBalance);
                    }
                    else if (lastOpeType == "deleteOperation")
                    {
                        PackageHost.SendMessage(MessageScope.Create("LedBank"), "SetGauge", currentBalance);
                    }
                } 
            }
        }

        /// <summary>
        /// Gets the current balance for person connected.
        /// </summary>
        /// <returns></returns>
        [MessageCallback]
        public double getCurrentBalanceForConnected()
        {
            return currentBalance;
        }
    }
}
