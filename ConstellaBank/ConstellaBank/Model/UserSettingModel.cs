﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstellaBank.Model
{
    public class UserSettingModel
    {
        /// <summary>
        /// Gets or sets the user setting model identifier.
        /// </summary>
        /// <value>
        /// The user setting model identifier.
        /// </value>
        public int UserSettingModelID { get; set; }

        /// <summary>
        /// Gets or sets the notification settings.
        /// </summary>
        /// <value>
        /// The notification settings.
        /// </value>
        public Dictionary<string, object> NotificationSettings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSettingModel"/> class.
        /// </summary>
        public UserSettingModel()
        {
            
        }
    }
}
