﻿var constellation = $.signalR.createConstellationConsumer("http://localhost:8088", "91bd5ba5e78d293abd320a24fc02ca53be149300", "ConstellaBank");
constellation.connection.start();

//Go to login by security: timeout session
var timeoutSession = localStorage.timeoutSession;

function clearLocalStorage() {
    localStorage.removeItem("firstName");
    localStorage.removeItem("accountName");
    localStorage.removeItem("personModelId");
    localStorage.removeItem("rememberMe");
    localStorage.removeItem("accountNumber");
    localStorage.removeItem("connectionDate");
}
function logOut() {
    clearLocalStorage();
    window.location = "login.html";
}
function checkIfTimeoutSession() {
    var now = moment().unix();
    var timeout = 600; //in seconds
    if ((now - localStorage.connectionDate > timeout || localStorage.timeoutSession == "true") && localStorage.rememberMe == "false") {
        localStorage.timeoutSession = true;
        logOut();
    }
}

if (localStorage.rememberMe == "false") {
    setInterval(function () { checkIfTimeoutSession(); }, 3000);
}

constellation.connection.stateChanged(function (change) {
    if (change.newState === $.signalR.connectionState.connected) {
        moment.locale('fr');

        //Récupérer les informations globales nécessaires du compte
        var accountNumber = localStorage.accountNumber;

        //Afficher le premier jour du mois dans les cartes "Débits" et "Crédits"
        var momentDate = new Date(), y = momentDate.getFullYear(), m = momentDate.getMonth();
        var firstDay = new Date(y, m, 1);
        firstDay = moment(firstDay).format("L");
        $("#debitFirstDay").html(firstDay);
        $("#creditFirstDay").html(firstDay);

        //Appels des méthodes
        if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null && timeoutSession == "false") {
            getAccountInfo(accountNumber);
            getAllOperationsForThisMonth(accountNumber);
            getFutureOperations(accountNumber);
            getTotalForFutureOperations(accountNumber);
            getFutureBalance(accountNumber);
            getTotalTreatedUntilToday(accountNumber, "DEBIT");
            getTotalTreatedUntilToday(accountNumber, "CREDIT");
            setTimeout(function () { retrieveSettings(accountNumber); }, 1000); //Le temps d'avoir les infos du localStorage
        }
        else {
            window.location = "login.html";
        }
 
        //////////////////////////////////////////METHODES//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////

        //Obtenir les informations du compte
        function getAccountInfo(accountNumber) {
            //Gauge
            var opts = {
                lines: 12,
                angle: 0.04,
                lineWidth: 0.02,
                pointer: {
                    length: 0,
                    strokeWidth: 0,
                    color: '#0288d1'
                },
                limitMax: 'false',
                percentColors: [[0.0, "#ff0000"], [0.50, "#f9c802"], [1.0, "#a9d70b"]],
                strokeColor: '#E0E0E0',
                generateGradient: true
            };

            var target = document.getElementById('gauge');
            gauge = new Gauge(target).setOptions(opts);
            gauge.maxValue = 3000;
            gauge.animationSpeed = 100;

            document.getElementById('gauge').style.width = "40%";
            document.getElementById('gauge').style.height = "45%";

            if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getAccountInfo', accountNumber, function (response) {
                    accountId = response.Data.AccountModelId;
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        //Balance
                        var balance = response.Data.Balance;
                        var maxValue = "3000";

                        if (balance >= maxValue) {
                            gauge.set(maxValue);
                        }
                        else if (balance <= 0) {
                            $("#preview-textfield").css('color', '#ff0000');
                            gauge.set(1);
                        }
                        else {
                            gauge.set(balance);
                        }
                        localStorage.setItem("firstName", response.Data.Users[0].Name);
                        localStorage.setItem("accountName", response.Data.AccountName);
                        localStorage.setItem("personModelId", response.Data.Users[0].PersonModelId);

                        $("#last_connection").html(moment(response.Data.DateLastConnection).format("LL"));
                        $("#name").html(response.Data.Users[0].Name);
                        $("#preview-textfield").html(response.Data.Balance.toFixed(2) + " €");
                        return accountId;
                    }
                });
            }
        }

        //Obtenir le solde à venir
        function getFutureBalance(accountNumber) {
            if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getFutureBalance', accountNumber, function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        $("#futureBalance").html(response.Data.toFixed(2) + " €");
                    }
                });
            }
        }

        //Obtenir le total des opérations futures
        function getTotalForFutureOperations(accountNumber) {
            if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getTotalForFutureOperations', accountNumber, function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        $("#totalFutureOperations").html(response.Data.toFixed(2) + " €");
                    }
                });
            }
        }

        //Obtenir le total des crédits/débits du mois jusqu'à aujourd'hui
        function getTotalTreatedUntilToday(accountNumber, operationType) {
            if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null && operationType != 'undefined' && operationType != '' && operationType != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getTotalTreatedForThisMonth', [accountNumber, operationType], function (response) {
                    if (operationType == "DEBIT") {
                        $("#totalDebits").html(response.Data.toFixed(2) + " €");
                    }
                    else {
                        $("#totalCredits").html(response.Data.toFixed(2) + " €");
                    }
                });
            }
        }

        //Obtenir les opérations du mois
        function getAllOperationsForThisMonth(accountNumber) {
            $(".noOperation").hide();
            $("#list_operations").show();
            if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getAllOperationsForThisMonth', accountNumber, function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        for (i = 0; i < response.Data.length; i++) {
                            if (response.Data[i].OperationType == 1 && response.Data[i].Recurrent == false) {
                                $('#list_operations').append('<tr bgcolor="#ffebee"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td>' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 0 && response.Data[i].Recurrent == false) {
                                $('#list_operations').append('<tr bgcolor="#e8f5e9"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td>' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 1 && response.Data[i].Recurrent == true) {
                                $('#list_operations').append('<tr bgcolor="#ffebee"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/></td><td><i class="material-icons prefix tiny">schedule</i> ' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 0 && response.Data[i].Recurrent == true) {
                                $('#list_operations').append('<tr bgcolor="#e8f5e9"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td><i class="material-icons prefix tiny">schedule</i> ' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                        }
                    }
                });
            }
        }

        //Obtenir les futures opérations
        function getFutureOperations(accountNumber) {
            if (accountNumber != 'undefined' && accountNumber != '' && accountNumber != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getFutureOperations', accountNumber, function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        for (i = 0; i < response.Data.length; i++) {
                            if (response.Data[i].OperationType == 1 && response.Data[i].Recurrent == false) {
                                $('#futureOperations').append('<tr bgcolor="#ffebee"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td>' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 0 && response.Data[i].Recurrent == false) {
                                $('#futureOperations').append('<tr bgcolor="#e8f5e9"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td>' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 1 && response.Data[i].Recurrent == true) {
                                $('#futureOperations').append('<tr bgcolor="#ffebee"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/></td><td><i class="material-icons prefix tiny">schedule</i> ' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 0 && response.Data[i].Recurrent == true) {
                                $('#futureOperations').append('<tr bgcolor="#e8f5e9"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td><i class="material-icons prefix tiny">schedule</i> ' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                        }
                    }
                });
            }
        }

        //Ajouter une opération
        function addOperation(accountNumber, title, price, category, operationType, operationRecurrent, date) {
            var personModelId = localStorage.personModelId;
            if (personModelId != "" && personModelId != "undefined" && personModelId != null &&
                accountNumber != "" && accountNumber != "undefined" && accountNumber != null &&
                title != "" && title != "undefined" && title != null &&
                price != "" && price != "undefined" && price != null &&
                date != "" && moment(date, ["DD/MM/YYYY"], true).isValid() &&
                category != null && category != "" && category != "undefined" &&
                operationType != null && operationType != "" && operationType != "undefined" &&
                operationRecurrent != null && operationRecurrent != "" && operationRecurrent != "undefined") {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'AddoperationInAccount', [accountNumber, personModelId, title.toString(), price, parseInt(category), operationType.toString(), operationRecurrent, date], function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        $('#modalAddOperation').closeModal();
                        window.location.reload();
                        Materialize.toast("L'opération a bien été ajoutée.", 3000, 'rounded')
                    }
                    else {
                        alert("Une erreur est survenue.");
                    }
                });
            }
            else {
                alert("Merci de remplir tous les champs.");
            }
        }
        $("#operationSubmit").click(function (event) {
            var date = moment($("#operationDate").val()).format("L");
            var operationType;
            var title = $("#title").val();
            var price = $("#montant").val();
            var category = $("#operationCat").val();
            var operationType = $("#operationType").val();
            var operationRecurrent = $("#operationRecurrent").val();

            if ($("#operationType").val() == "0") {
                operationType = "DEBIT";
            }
            else {
                operationType = "CREDIT";
            }
            checkIfTimeoutSession();
            addOperation(accountNumber, title, price, category, operationType, operationRecurrent, date);
        });

        //Modal: Voir les détails d'une opération
        function getOperationDetails(operationId, operationType, recurrent, date, category, title, price) {
            if (recurrent != "" && recurrent != "undefined" && recurrent != null &&
                operationId != "" && operationId != "undefined" && operationId != null &&
                operationType != "" && operationType != "undefined" && operationType != null &&
                date != "" && date != "undefined" && date != null &&
                category != "" && category != "undefined" && category != null &&
                title != "" && title != "undefined" && title != null &&
                price != "" && price != "undefined" && price != null) {

                $("#editTitle").val(title);
                $("#editPrice").val(price);
                $('#editCat').val(category);
                $("#editDate").val(date);
                //Débit et crédit sont inversés entre le front et le back.
                if (operationType == 1) {
                    operationType = 0;
                }
                else {
                    operationType = 1;
                }
                $("#editOperationType").val(operationType);
                $("#editOperationRecurrent").val(recurrent);

                // re-initialize material-select
                $('#editCat').material_select();
                $('#editOperationType').material_select();
                $('#editOperationRecurrent').material_select();
                $('#editOperation').openModal();
            }
        }
        $("#list_operations, #futureOperations, #search_list_operations").on("click", "td", function () {
            operationId = $(this).closest("tr").children("td")[0].children[0].defaultValue;
            var operationType = $(this).closest("tr").children("td")[1].children[0].defaultValue;
            var recurrent = $(this).closest("tr").children("td")[2].children[0].defaultValue;
            var date = $(this).closest("tr").children("td")[3].innerHTML;
            var formatedDate = moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD');
            var category = $(this).closest("tr").children("td")[4].firstElementChild.attributes[0].nodeValue;
            var title = $(this).closest("tr").children("td")[5].innerHTML;
            var price = $(this).closest("tr").children("td")[6].innerHTML;
            var formatedPrice = price.replace('€', '');
            formatedPrice = formatedPrice.replace(/\s/g, '');

            getOperationDetails(operationId, operationType, recurrent, formatedDate, category, title, formatedPrice);
        });

        //Modal : Editer une opération
        function editOperation(operationId, editTitle, editPrice, editCategory, editOperationType, editOperationRecurrent, date) {
            if (editTitle != "" && editTitle != "undefined" && editTitle != null && editPrice != "" && editPrice != "undefined" && editPrice != null && editCategory != "" && editCategory != "undefined" && editCategory != null && editDate != "" && editDate != "undefined" && editDate != null && operationId != "" && operationId != "undefined" && operationId != null && editOperationRecurrent != null && editOperationRecurrent != "undefined" && editOperationRecurrent != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'updateOperation', [operationId, editTitle.toString(), editPrice, parseInt(editCategory), editOperationType.toString(), editOperationRecurrent, date], function (response) {
                    if (response.Data == true) {
                        $('#editOperation').closeModal();
                        window.location.reload();
                        Materialize.toast("L'opération a bien été modifiée.", 3000, 'rounded')
                    }
                    else {
                        alert("Une erreur est survenue.");
                    }
                });
            }
            else {
                alert("Merci de remplir tous les champs.");
            }
        }
        $("#operationEdit").click(function (event) {
            var editDate = $("#editDate").val();
            var date = moment(editDate).format("L");
            var editTitle = $("#editTitle").val();
            var editPrice = $("#editPrice").val();
            var editCategory = $("#editCat").val();
            var editOperationRecurrent = $("#editOperationRecurrent").val();

            if ($("#editOperationType").val() == "0") {
                editOperationType = "DEBIT";
            }
            else {
                editOperationType = "CREDIT";
            }
            checkIfTimeoutSession();
            editOperation(operationId, editTitle, editPrice, editCategory, editOperationType, editOperationRecurrent, date);
        });

        //Modal : Supprimer une opération
        function deleteOperation(operationId) {
            if (operationId != null && operationId != "" && operationId != "undefined") {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'deleteOperation', operationId, function (response) {
                    if (response.Data == true) {
                        $('#editOperation').closeModal();
                        window.location.reload();
                        Materialize.toast("L'opération a bien été supprimée.", 3000, 'rounded')
                    }
                    else {
                        alert("Une erreur est survenue.");
                    }
                });
            }
            else {
                alert("L'opération n'existe pas.");
            }
        }
        $("#operationDelete").click(function (event) {
            checkIfTimeoutSession();
            deleteOperation(operationId);
        });

        //Rechercher une opération
        function searchOperation(accountNumber, beginDate, endDate) {
            if (accountNumber != "" && accountNumber != "undefined" && accountNumber != null && moment(beginDate, ["DD/MM/YYYY"], true).isValid() && moment(endDate, ["DD/MM/YYYY"], true).isValid() && endDate >= beginDate) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getOperationsFromDateToDate', [accountNumber, beginDate, endDate], function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        $("#list_operations").hide();
                        $("#noOperation").hide();
                        $("#search_list_operations tbody").remove();
                        $("#search_list_operations").show();
                        for (i = 0; i < response.Data.length; i++) {
                            if (response.Data[i].OperationType == 1 && response.Data[i].Recurrent == false) {
                                $('#search_list_operations').append('<tr bgcolor="#ffebee"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td>' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 0 && response.Data[i].Recurrent == false) {
                                $('#search_list_operations').append('<tr bgcolor="#e8f5e9"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td>' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 1 && response.Data[i].Recurrent == true) {
                                $('#search_list_operations').append('<tr bgcolor="#ffebee"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/></td><td><i class="material-icons prefix tiny">schedule</i> ' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                            else if (response.Data[i].OperationType == 0 && response.Data[i].Recurrent == true) {
                                $('#search_list_operations').append('<tr bgcolor="#e8f5e9"><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].OperationModelId + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation2" name="hiddenOperation2" value="' + response.Data[i].OperationType + '"/></td><td style="display:none"><input type="hidden" class="hidenOperation" name="hiddenOperation" value="' + response.Data[i].Recurrent + '"/><td><i class="material-icons prefix tiny">schedule</i> ' + moment(response.Data[i].OperationDate).format("L") + '</td><td><img value="' + response.Data[i].Category.CategoryModelID + '" src="images/' + response.Data[i].Category.CategoryName.toLowerCase() + '.png" height="16" width="16">  ' + response.Data[i].Category.CategoryName + '</td><td>' + response.Data[i].Title + '</td><td>' + response.Data[i].Price + ' €</td></tr>');
                            }
                        }
                    }
                    else {
                        $("#list_operations").hide();
                        $("#search_list_operations").hide();
                        $("#noOperation").html("Pas d'opération disponible.");
                    }
                });
            }
            else {
                alert("Merci de remplir correctement tous les champs.");
            }
        }
        $("#searchDate").click(function (event) {
            event.preventDefault();
            moment.locale('fr');
            var beginDate = moment($("#beginSearchDate").val()).format("L");
            var endDate = moment($("#endSearchDate").val()).format("L");
            checkIfTimeoutSession();
            searchOperation(accountNumber, beginDate, endDate);
        });

        //////////////////////////////////****Réglages****//////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        function retrieveNotificationSettings(personModelId) {
            if (personModelId != "" && personModelId != "undefined" && personModelId != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'getSettingsForUser', personModelId, function (response) {
                    if (response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        var switchAllOperations = response.Data.NotificationSettings.AllOperations;
                        var switchCreditOperations = response.Data.NotificationSettings.OnlyCreditOperations;
                        var switchDebitOperations = response.Data.NotificationSettings.OnlyDebitOperations;
                        var limitBalance = response.Data.NotificationSettings.AccountCeiling;
                        var limitCreditOperations = response.Data.NotificationSettings.CreditCeiling;
                        var limitDebitOperations = response.Data.NotificationSettings.DebitCeiling;

                        if (switchAllOperations != "undefined" &&
                            switchCreditOperations != "undefined" &&
                            switchDebitOperations != "undefined" &&
                            limitBalance != "undefined" &&
                            limitCreditOperations != "undefined" &&
                            limitDebitOperations != "undefined") {

                            $("#allOperations").prop('checked', switchAllOperations);
                            $("#creditOperations").prop('checked', switchCreditOperations);
                            $("#debitOperations").prop('checked', switchDebitOperations);
                            $("#balanceLimit").val(limitBalance);
                            $("#creditOperationsLimit").val(limitCreditOperations);
                            $("#debitOperationsLimit").val(limitDebitOperations);
                        }
                    }
                });
            }
        }

        function retrieveSettings(accountNumber) {
            var personModelId = localStorage.personModelId;
            if (personModelId != "" && personModelId != "undefined" && personModelId != null &&
                accountNumber != 'undefined' && accountNumber != '' && accountNumber != null) {
                $("#editNameAccount").val(localStorage.accountName);
                $("#editFirstName").val(localStorage.firstName);
                retrieveNotificationSettings(personModelId);
            }
        }

        //Notifications
        //Désactiver certains switchs
        $("#allOperations").change(function () {
            if ($("#allOperations").is(':checked')) {
                $("#creditOperations").prop('disabled', true);
                $("#debitOperations").prop('disabled', true);
                $("#balanceLimit").val("");
                $("#balanceLimit").prop('disabled', true);
                $("#creditOperationsLimit").val("");
                $("#creditOperationsLimit").prop('disabled', true);
                $("#debitOperationsLimit").val("");
                $("#debitOperationsLimit").prop('disabled', true);
            }
            else {
                $("#creditOperations").prop('disabled', false);
                $("#debitOperations").prop('disabled', false);
                $("#balanceLimit").prop('disabled', false);
                $("#creditOperationsLimit").prop('disabled', false);
                $("#debitOperationsLimit").prop('disabled', false);
            }
        });
        $("#creditOperations").change(function () {
            if ($("#creditOperations").is(':checked')) {
                $("#allOperations").prop('disabled', true);
            }
            else if ($("#debitOperations").is(':checked') == false) {
                $("#allOperations").prop('disabled', false);
            }
        });
        $("#debitOperations").change(function () {
            if ($("#debitOperations").is(':checked')) {
                $("#allOperations").prop('disabled', true);
            }
            else if ($("#creditOperations").is(':checked') == false) {
                $("#allOperations").prop('disabled', false);
            }
        });

        //Mettre à jour les réglages de notifications
        function updateNotificationSettings(personModelId, switchAllOperations, switchCreditOperations, switchDebitOperations, limitBalance, limitCreditOperations, limitDebitOperations, changeAccountName, changeFirstName, changePwd, changeConfirmPwd) {
            if (personModelId != "" && personModelId != "undefined" && personModelId != null &&
                switchAllOperations != "undefined" && switchAllOperations != null &&
                switchCreditOperations != "undefined" && switchCreditOperations != null &&
                switchDebitOperations != "undefined" && switchDebitOperations != null &&
                limitBalance != "undefined" && limitBalance != null &&
                limitCreditOperations != "undefined" && limitCreditOperations != null &&
                limitDebitOperations != "undefined" && limitDebitOperations != null) {

                var notificationsDico = {
                    "AllOperations": switchAllOperations,
                    "OnlyDebitOperations": switchDebitOperations,
                    "OnlyCreditOperations": switchCreditOperations,
                    "AccountCeiling": parseInt(limitBalance),
                    "CreditCeiling": parseInt(limitCreditOperations),
                    "DebitCeiling": parseInt(limitDebitOperations)
                };

                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'updateUserNotificationSettings', [personModelId, notificationsDico], function (response) {
                    if (response.Data == true) {
                        Materialize.toast('Modification effectuée avec succès !', 4000, 'rounded green');
                        location.reload(2);
                    }
                    else {
                        alert("Une erreur est survenue.");
                    }
                });
            }
            else {
                alert("Merci de remplir tous les champs.");
            }
        }
        $("#editNotificationSettings").click(function (event) {
            event.preventDefault();
            var switchAllOperations = $("#allOperations").is(':checked');
            var switchCreditOperations = $("#creditOperations").is(':checked');
            var switchDebitOperations = $("#debitOperations").is(':checked');
            var limitBalance = $("#balanceLimit").val();
            var limitCreditOperations = $("#creditOperationsLimit").val();
            var limitDebitOperations = $("#debitOperationsLimit").val();
            var personModelId = localStorage.personModelId;

            checkIfTimeoutSession();
            updateNotificationSettings(personModelId, switchAllOperations, switchCreditOperations, switchDebitOperations, limitBalance, limitCreditOperations, limitDebitOperations);
        });

        //Modifier le nom du compte
        function updateAccountNameSettings(accountNumber, changeAccountName) {
            if (accountNumber != "" && accountNumber != "undefined" && accountNumber != null &&
                changeAccountName != "" && changeAccountName != "undefined" && changeAccountName != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'changeAccountName', [accountNumber, changeAccountName], function (response) {
                    if (response.Data == true) {
                        $('#modalSettings').closeModal();
                        location.reload();
                        Materialize.toast('Modification effectuée avec succès !', 4000, 'rounded green');
                        location.reload(2);
                    }
                    else {
                        alert("Une erreur est survenue.");
                    }
                });
            }
            else {
                alert("Merci de remplir tous les champs.");
            }
        }
        $("#editAccountNameSettings").click(function (event) {
            var changeAccountName = $("#editNameAccount").val();
            checkIfTimeoutSession();
            updateAccountNameSettings(accountNumber, changeAccountName);
        });

        function updateFirstNameSettings(personModelId, changeFirstName) {
            if (personModelId != "" && personModelId != "undefined" && personModelId != null &&
                changeFirstName != "" && changeFirstName != "undefined" && changeFirstName != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'changeUserName', [personModelId, changeFirstName], function (response) {
                    if (response.Data == true) {
                        Materialize.toast('Modification effectuée avec succès !', 4000, 'rounded green');
                        location.reload(2);
                    }
                    else {
                        alert("Une erreur est survenue.");
                    }
                });
            }
            else {
                alert("Merci de remplir tous les champs.");
            }
        }
        $("#editFirstNameSettings").click(function (event) {
            var changeFirstName = $("#editFirstName").val();
            var personModelId = localStorage.personModelId;
            checkIfTimeoutSession();
            updateFirstNameSettings(personModelId, changeFirstName);
        });

        //Modifier le mot de passe
        function updatePasswordSettings(changePwd, changeConfirmPwd) {
            var sha1Pwd = sha1(changeConfirmPwd);
            var personModelId = localStorage.personModelId;
            if (accountNumber != "" && accountNumber != "undefined" && accountNumber != null &&
            changePwd != "" && changePwd != "undefined" && changePwd != null &&
            changeConfirmPwd != "" && changeConfirmPwd != "undefined" && changeConfirmPwd != null
            && sha1Pwd != "undefined" && sha1Pwd != "" && sha1Pwd != null
            && personModelId != "undefined" && personModelId != "" && personModelId != null) {

                if (changePwd != changeConfirmPwd) {
                    alert("Les mots de passe ne sont pas identiques.");
                }
                else {
                    constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'changeUserPassword', [personModelId, sha1Pwd], function (response) {
                        if (response.Data == true) {
                            Materialize.toast('Le mot de passe a bien été modifié !', 4000, 'rounded green');
                            location.reload(2);
                        }
                        else {
                            alert("Une erreur est survenue.");
                        }
                    });
                }
            }
            else {
                alert("Merci de remplir tous les champs.");
            }
        }
        $("#editPasswordSettings").click(function (event) {
            var changePwd = $("#editPwd").val();
            var changeConfirmPwd = $("#confirmEditPwd").val();
            checkIfTimeoutSession();
            updatePasswordSettings(changePwd, changeConfirmPwd);
        });

        //Déconnexion
        $(".logOut").click(function (event) {
            event.preventDefault();
            logOut();
        });
    }
});