﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ConstellaBank.Enums;
using ConstellaBank.Model;
using Constellation.Package;

namespace ConstellaBank.StateObjectModel
{
    [StateObject]
    public class OperationModel
    {
        /// <summary>
        /// Gets or sets the operation model identifier.
        /// </summary>
        /// <value>
        /// The operation model identifier.
        /// </value>
        public int OperationModelId { get; set; }
        /// <summary>
        /// Gets or sets the executant.
        /// </summary>
        /// <value>
        /// The executant.
        /// </value>
        public PersonModel Executant { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public Double Price { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="OperationModel"/> is recurrent.
        /// </summary>
        /// <value>
        ///   <c>true</c> if recurrent; otherwise, <c>false</c>.
        /// </value>
        public bool Recurrent { get; set; }
        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        public CategoryModel Category { get; set; }
        /// <summary>
        /// Gets or sets the operation date.
        /// </summary>
        /// <value>
        /// The operation date.
        /// </value>
        public DateTime OperationDate { get; set; }
        /// <summary>
        /// Gets or sets the type of the operation.
        /// </summary>
        /// <value>
        /// The type of the operation.
        /// </value>
        public OperationType OperationType { get; set; }
        /// <summary>
        /// Gets or sets for account.
        /// </summary>
        /// <value>
        /// For account.
        /// </value>
        public AccountModel ForAccount { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is treated.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is treated; otherwise, <c>false</c>.
        /// </value>
        public bool isTreated { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationModel"/> class.
        /// </summary>
        /// <param name="executant">The executant.</param>
        /// <param name="title">The title.</param>
        /// <param name="price">The price.</param>
        /// <param name="recurrent">if set to <c>true</c> [recurrent].</param>
        /// <param name="category">The category.</param>
        /// <param name="forAccount">For account.</param>
        /// <param name="operationDate">The operation date.</param>
        /// <param name="operationType">Type of the operation.</param>
        public OperationModel(PersonModel executant, string title, Double price, bool recurrent, CategoryModel category, AccountModel forAccount, DateTime operationDate, OperationType operationType)
        {
            Executant = executant;
            Title = title;
            Price = price;
            Recurrent = recurrent;
            OperationDate = operationDate;
            OperationType = operationType;
            ForAccount = forAccount;
            isTreated = false;
            Category = category;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OperationModel"/> class.
        /// </summary>
        public OperationModel()
        {
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return
                $"Operation - Title : {Title}, Price : {Price}, By : {Executant.Name}, Date : {OperationDate}, Category : {Category.CategoryName}, Type : {OperationType}, Account : {ForAccount.AccountName}";
        }
    }
}
