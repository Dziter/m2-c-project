﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstellaBank.Enums
{
    /// <summary>
    /// Enum for the operation type
    /// </summary>
    public enum OperationType
    {
        CREDIT,
        DEBIT
    }
}
