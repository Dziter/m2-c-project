﻿using Constellation.Package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstellaBank.StateObjectModel
{
    [StateObject]
    public class AccountModel
    {
        /// <summary>
        /// Gets or sets the account model identifier.
        /// </summary>
        /// <value>
        /// The account model identifier.
        /// </value>
        public int AccountModelId { get; set; }
        /// <summary>
        /// Gets or sets the name of the account.
        /// </summary>
        /// <value>
        /// The name of the account.
        /// </value>
        public string AccountName { get; set; }
        /// <summary>
        /// Gets or sets the balance.
        /// </summary>
        /// <value>
        /// The balance.
        /// </value>
        public Double Balance { get; set; }
        /// <summary>
        /// Gets or sets the date last connection.
        /// </summary>
        /// <value>
        /// The date last connection.
        /// </value>
        public DateTime DateLastConnection { get; set; }
        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
        public List<PersonModel> Users { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountModel"/> class.
        /// </summary>
        /// <param name="balance">The balance.</param>
        /// <param name="name">The name.</param>
        public AccountModel(Double balance, string name)
        {
            Balance = balance;
            DateLastConnection = DateTime.Now;
            AccountName = name;
            Users = new List<PersonModel>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountModel"/> class.
        /// </summary>
        public AccountModel()
        {
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Account - Id : {0}, Name : {1}, Balance : {2}", AccountModelId, AccountName, Balance);
        }
    }
}