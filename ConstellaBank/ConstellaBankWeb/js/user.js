﻿var constellation = $.signalR.createConstellationConsumer("http://localhost:8088", "91bd5ba5e78d293abd320a24fc02ca53be149300", "ConstellaBank");
constellation.connection.start();

constellation.connection.stateChanged(function (change) {
    if (change.newState === $.signalR.connectionState.connected) {

        //Créer un compte
        $("#createAccountButton").click(function (event) {
            event.preventDefault();
            var firstName = $("#newAccountFirstname").val();
            var nickName = $("#newAccountNickname").val();
            var pwd = $("#newAccountPassword").val();
            var confirmPwd = $("#newAccountConfirmPassword").val();
            var sha1Pwd = sha1(confirmPwd);

            if (firstName != "undefined" && firstName != "" && firstName != null && nickName != "undefined" && nickName != "" && nickName != null && pwd != "undefined" && pwd != "" && pwd != null && confirmPwd != "undefined" && confirmPwd != "" && confirmPwd != null && pwd == confirmPwd && sha1Pwd != "undefined" && sha1Pwd != "" && sha1Pwd != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'createNewUser', [firstName, nickName, sha1Pwd], function (response) {
                    if (response.Data == true) {
                        Materialize.toast('Compte créé avec succès !', 4000, 'rounded green');
                        $("#cardAccount").hide();
                        $("#cardLogin").show();
                    }
                    else {
                        Materialize.toast("Nom d'utilisateur déjà existant", 4000, 'rounded red');
                    }
                });
            }
            else if (pwd != confirmPwd) {
                Materialize.toast('Les deux mots de passe ne sont pas identiques.', 4000);
            }
            else {
                Materialize.toast('Merci de remplir tous les champs.', 4000);
            }
        });

        //Se connecter
        $("#loginButton").click(function (event) {
            event.preventDefault();
            var nickName = $("#loginNickname").val();
            var pwd = $("#loginPassword").val();
            var sha1Pwd = sha1(pwd);

            if (nickName != "undefined" && nickName != "" && nickName != null && pwd != "undefined" && pwd != "" && pwd != null && sha1Pwd != "undefined" && sha1Pwd != "" && sha1Pwd != null) {
                constellation.server.sendMessageWithSaga({ Scope: 'Package', Args: ['ConstellaBank'] }, 'loginUser', [nickName, sha1Pwd], function (response) {
                    if (response.Data != -1 && response.Data != "" && response.Data != "undefined" && response.Data != null) {
                        //Mémorisation de la connexion
                        if ($('#rememberMe').prop('checked')) {
                            localStorage.setItem('rememberMe', true);
                        }
                        else {
                            localStorage.setItem('rememberMe', false);
                        }
                        localStorage.setItem('accountNumber', response.Data);
                        localStorage.setItem('connectionDate', moment().unix());
                        localStorage.setItem('timeoutSession', false);
                        window.location = "Default.html";
                    }
                    else {
                        Materialize.toast('Identifiants invalides.', 4000, 'rounded red');
                    }
                });
            }
            else {
                Materialize.toast('Merci de remplir tous les champs.', 4000, 'rounded red');
            }
        });
    }
});