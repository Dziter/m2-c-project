﻿using ConstellaBank.StateObjectModel;
using Constellation;
using Constellation.Package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConstellaBank.Enums;
using ConstellaBank.Model;
using LiteDB;

namespace ConstellaBank
{
    public class Program : PackageBase
    {
        #region Vars

        /// <summary>
        /// The URL of the database
        /// </summary>
        private static readonly string urlDatabase = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                                     @"\Constellabank.db";

        //Timer de mise a jour des balances
        public Timer TimerUpdateBalance { get; set; }
        //TimeSpan - Intervalle de temps pour le lancement du trigger de vérification de date pour la mise à jour des balances
        public TimeSpan TimeSpanUpdateBalances { get; } = new TimeSpan(0, 1, 0, 0);
        //Date de dernière vérification pour le timer de mise à jour des balances
        public DateTime LastDateChecked { get; set; }
        public BsonMapper BsonMapper { get; } = BsonMapper.Global;

        /// <summary>
        /// Gets the default notification ditionnary.
        /// </summary>
        /// <value>
        /// The default notification ditionnary.
        /// </value>
        public Dictionary<string, object> DefaultNotificationDitionnary { get; } = new Dictionary<string, object>()
        {
            {"AllOperations", true},
            {"OnlyDebitOperations", false},
            {"OnlyCreditOperations", false},
            {"AccountCeiling", 0},
            {"CreditCeiling", 0},
            {"DebitCeiling", 0}
        };

        /// <summary>
        /// Gets or sets the login state object.
        /// </summary>
        /// <value>
        /// The login state object.
        /// </value>
        public dynamic LoginStateObject { get; set; }

        #endregion

        static void Main(string[] args)
        {
            PackageHost.Start<Program>(args);
        }

        #region Setup Constellabank

        /// <summary>
        /// Called when the package is started.
        /// </summary>
        public override void OnStart()
        {
            PackageHost.WriteInfo("Package starting - IsRunning: {0} - IsConnected: {1}", PackageHost.IsRunning,
                PackageHost.IsConnected);

            //Ajout des informations dans la BDD
            addSampleInformation();
            //Mise en place du timer de mise à jour des balances des comptes
            setupTimerUpdateBalances();
            //Premier update des balances au lancement du package afin de passer les opérations antérieures et du jour
            updateAllBalances();
        }

        /// <summary>
        /// Adds the sample information.
        /// </summary>
        public void addSampleInformation()
        {
            //Create the tests objects

            UserSettingModel settingNicolas = new UserSettingModel()
            {
                NotificationSettings = DefaultNotificationDitionnary
            };

            UserSettingModel settingBaptise = new UserSettingModel()
            {
                NotificationSettings = DefaultNotificationDitionnary
            };

            PersonModel nicolas = new PersonModel()
            {
                LoginName = "nicolasC",
                Name = "Nicolas",
                Password = "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8",
                UserSettings = settingNicolas
            };
            PersonModel baptiste = new PersonModel()
            {
                LoginName = "baptisteK",
                Name = "Baptiste",
                Password = "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8",
                UserSettings = settingBaptise
            };
            AccountModel compteNicolas = new AccountModel()
            {
                AccountName = "Compte Nicolas",
                Balance = 3000,
                DateLastConnection = DateTime.Now,
                Users = new List<PersonModel>() {nicolas}
            };
            AccountModel compteBaptiste = new AccountModel()
            {
                AccountName = "Compte Baptiste",
                Balance = 3000,
                DateLastConnection = DateTime.Now,
                Users = new List<PersonModel>() {baptiste}
            };
            CategoryModel shoppingCategory = new CategoryModel("Shopping");
            CategoryModel alimentationCategory = new CategoryModel("Alimentation");
            CategoryModel loisirsCategory = new CategoryModel("Loisirs");
            CategoryModel bourseCategory = new CategoryModel("Bourse");
            CategoryModel transportCategory = new CategoryModel("Transport");
            CategoryModel factureCategory = new CategoryModel("Facture");
            CategoryModel salaireCategory = new CategoryModel("Salaire");


            OperationModel achatPolisseuse = new OperationModel(nicolas, "Achat Polisseuse", 230, false,
                shoppingCategory, compteNicolas, DateTime.Now, OperationType.DEBIT);
            OperationModel achatIphone = new OperationModel(baptiste, "Achat iPhone 7", 550, false, shoppingCategory,
                compteBaptiste, DateTime.Now, OperationType.DEBIT);
            OperationModel salaireNico = new OperationModel(nicolas, "Les biftons", 4500, false, salaireCategory,
                compteNicolas, DateTime.Now, OperationType.CREDIT);

            using (var db = new LiteDatabase(urlDatabase))
            {
                BsonMapper.Entity<OperationModel>().DbRef(x => x.Executant, "Users");
                BsonMapper.Entity<OperationModel>().DbRef(z => z.ForAccount, "Accounts");
                BsonMapper.Entity<OperationModel>().DbRef(z => z.Category, "Categories");
                BsonMapper.Entity<AccountModel>().DbRef(x => x.Users, "Users");
                BsonMapper.Entity<PersonModel>().DbRef(x => x.UserSettings, "Settings");
                //BsonMapper.Entity<PersonModel>().Index(x => x.LoginName, true);

                var settings = db.GetCollection<UserSettingModel>("Settings");
                if (settings.Count() == 0)
                {
                    settings.Insert(settingNicolas);
                    settings.Insert(settingBaptise);
                }

                var users = db.GetCollection<PersonModel>("Users");
                if (users.Count() == 0)
                {
                    users.Insert(nicolas);
                    users.Insert(baptiste);
                }

                var account = db.GetCollection<AccountModel>("Accounts");
                if (account.Count() == 0)
                {
                    account.Insert(compteBaptiste);
                    account.Insert(compteNicolas);
                }

                var categories = db.GetCollection<CategoryModel>("Categories");
                if (categories.Count() == 0)
                {
                    categories.Insert(shoppingCategory);
                    categories.Insert(alimentationCategory);
                    categories.Insert(loisirsCategory);
                    categories.Insert(bourseCategory);
                    categories.Insert(transportCategory);
                    categories.Insert(factureCategory);
                    categories.Insert(salaireCategory);
                }

                var operations = db.GetCollection<OperationModel>("Operations");
                if (operations.Count() == 0)
                {
                    operations.Insert(achatIphone);
                    operations.Insert(achatPolisseuse);
                    operations.Insert(salaireNico);
                }
            }
        }

        #endregion

        #region Time update balances

        /// <summary>
        /// Setups the timer update balances.
        /// </summary>
        public void setupTimerUpdateBalances()
        {
            LastDateChecked = DateTime.Now;
            //Timer de mise à jour quotidien du solde de compte
            TimerUpdateBalance = new Timer(state => triggerUpdateBalance(), null, TimeSpan.Zero, TimeSpanUpdateBalances);
            //Afin d'éviter que le timer soit ramassé par le Garbage Collector
            GC.KeepAlive(TimerUpdateBalance);
        }

        /// <summary>
        /// Triggers the update balance.
        /// </summary>
        public void triggerUpdateBalance()
        {
            PackageHost.WriteInfo($"Trigger : {DateTime.Now}");
            if (LastDateChecked.Date < DateTime.Now.Date)
            {
                PackageHost.WriteInfo("New day in trigger");
                //Si on passe au jour suivant lors de la vérification alors on lance les mises à jours des balances des comptes
                updateAllBalances();
            }
            //On passe la valeur à la date et heure actuelle
            LastDateChecked = DateTime.Now;
        }

        #endregion

        #region Info getters

        /// <summary>
        /// Gets all operations.
        /// </summary>
        /// <returns></returns>
        [MessageCallback]
        public List<OperationModel> getAllOperations()
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    var operations = db.GetCollection<OperationModel>("Operations")
                        .Include(z => z.Category)
                        .FindAll();
                    return operations.OrderByDescending(x => x.OperationDate).ToList();
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error : {e.Message}");
                return null;
            }
        }

        /// <summary>
        /// Gets the account information.
        /// </summary>
        /// <param name="accountId">The account identifier.</param>
        /// <returns></returns>
        [MessageCallback(ResponseType = typeof(AccountModel))]
        public AccountModel getAccountInfo(int accountId)
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                return
                    db.GetCollection<AccountModel>("Accounts")
                        .Include(x => x.Users)
                        .FindById(accountId);
            }
        }

        /// <summary>
        /// Gets the operations for this month.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        [MessageCallback]
        public List<OperationModel> getOperationsForThisMonth(int account, OperationType type)
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                var operations = db.GetCollection<OperationModel>("Operations")
                    .Include(z => z.Category)
                    .FindAll()
                    .Where(
                        e =>
                            e.ForAccount.AccountModelId == account && e.OperationType == type &&
                            e.OperationDate.Month == DateTime.Now.Month);

                return operations.OrderByDescending(x => x.OperationDate).ToList();
            }
        }

        /// <summary>
        /// Gets all operations for this month.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <returns></returns>
        [MessageCallback]
        public List<OperationModel> getAllOperationsForThisMonth(int account)
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                var operations = db.GetCollection<OperationModel>("Operations")
                    .Include(z => z.Category)
                    .FindAll()
                    .Where(
                        e =>
                            e.ForAccount.AccountModelId == account &&
                            e.OperationDate.Month == DateTime.Now.Month && e.isTreated);
                return operations.OrderByDescending(x => x.OperationDate).ToList();
            }
        }

        /// <summary>
        /// Gets the operations from date to date.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="dateBeginString">The date begin string.</param>
        /// <param name="dateEndString">The date end string.</param>
        /// <returns></returns>
        [MessageCallback]
        public List<OperationModel> getOperationsFromDateToDate(int account, string dateBeginString,
            string dateEndString)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    DateTime dateBegin = Convert.ToDateTime(dateBeginString);
                    DateTime dateEnd = Convert.ToDateTime(dateEndString);
                    var operations = db.GetCollection<OperationModel>("Operations")
                        .Include(z => z.Category)
                        .FindAll()
                        .Where(
                            e =>
                                e.ForAccount.AccountModelId == account
                                && e.OperationDate.Date >= dateBegin
                                && e.OperationDate.Date <= dateEnd);
                    return operations.OrderByDescending(x => x.OperationDate).ToList();
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while getting date to date operations : {e.Message}");
                return null;
            }
        }

        /// <summary>
        /// Gets the future operations.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <returns></returns>
        [MessageCallback]
        public List<OperationModel> getFutureOperations(int account)
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                var operations = db.GetCollection<OperationModel>("Operations")
                    .Include(z => z.Category)
                    .Find(e => e.ForAccount.AccountModelId == account && e.OperationDate > DateTime.Now);
                return operations.OrderBy(model => model.OperationDate).ToList();
            }
        }

        /// <summary>
        /// Gets the total for future operations.
        /// </summary>
        /// <param name="accountId">The account identifier.</param>
        /// <returns></returns>
        [MessageCallback]
        public double getTotalForFutureOperations(int accountId)
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                double total = 0;
                var operations = db.GetCollection<OperationModel>("Operations")
                    .FindAll()
                    .Where(z => z.ForAccount.AccountModelId == accountId && z.OperationDate.Date > DateTime.Now.Date);
                foreach (OperationModel operation in operations)
                {
                    total = computeDebitOrCredit(operation, total, false);
                }
                return total;
            }
        }

        /// <summary>
        /// Gets the future balance.
        /// </summary>
        /// <param name="accountId">The account identifier.</param>
        /// <returns></returns>
        [MessageCallback]
        public double getFutureBalance(int accountId)
        {
            AccountModel account;
            using (var db = new LiteDatabase(urlDatabase))
            {
                account = db.GetCollection<AccountModel>("Accounts").FindById(accountId);
            }
            double futureOpeTotal = getTotalForFutureOperations(accountId);
            return account.Balance + futureOpeTotal;
        }

        /// <summary>
        /// Gets the total operation treated for this month.
        /// </summary>
        /// <param name="accountID">The account identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        [MessageCallback]
        public double getTotalTreatedForThisMonth(int accountID, OperationType type)
        {
            var operations = getListTreatedOperationForThisMonth(accountID,
                type == OperationType.CREDIT ? OperationType.CREDIT : OperationType.DEBIT);
            double total = 0;
            foreach (OperationModel operation in operations)
            {
                total += operation.Price;
            }
            return total;
        }

        /// <summary>
        /// Gets the list treated operation for this month.
        /// </summary>
        /// <param name="accountId">The account identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public List<OperationModel> getListTreatedOperationForThisMonth(int accountId, OperationType type)
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                var operations = db.GetCollection<OperationModel>("Operations")
                    .FindAll()
                    .Where(
                        z =>
                            z.ForAccount.AccountModelId == accountId && z.OperationType == type &&
                            z.OperationDate.Month == DateTime.Now.Month &&
                            z.isTreated);
                return operations.OrderBy(model => model.OperationDate).ToList();
            }
        }

        #endregion

        #region Info setters

        /// <summary>
        /// Add operations in account.
        /// </summary>
        /// <param name="accountId">The account identifier.</param>
        /// <param name="personId">The person identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="price">The price.</param>
        /// <param name="categoryID">The category identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="recurrent">if set to <c>true</c> [recurrent].</param>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        [MessageCallback]
        public OperationModel AddoperationInAccount(int accountId, int personId, string title, Double price,
            int categoryID, OperationType type, bool recurrent, string date)
        {
            OperationModel newOperation;
            using (var db = new LiteDatabase(urlDatabase))
            {
                try
                {
                    AccountModel account = db.GetCollection<AccountModel>("Accounts").FindById(accountId);
                    PersonModel person = db.GetCollection<PersonModel>("Users").FindById(personId);
                    CategoryModel category = db.GetCollection<CategoryModel>("Categories").FindById(categoryID);
                    var operations = db.GetCollection<OperationModel>("Operations");
                    DateTime dateOperation = Convert.ToDateTime(date);
                    newOperation = new OperationModel(person, title, price, recurrent, category, account, dateOperation,
                        type);
                    operations.Insert(newOperation);
                    PackageHost.WriteInfo($"Operation added : {newOperation}");
                    updateBalance(account, newOperation, db);
                    //notifyLed(newOperation);
                    sendNotificationDebitOrCredit(personId, newOperation);
                    sendNotificationAccountCeiling(personId, account);
                    return newOperation;
                }
                catch (Exception e)
                {
                    PackageHost.WriteError($"Problem adding new operation : {e.Message}");
                    return null;
                }
            }
        }

        /// <summary>
        /// Deletes all operations.
        /// </summary>
        [MessageCallback]
        public void deleteAllOperations()
        {
            using (var db = new LiteDatabase(urlDatabase))
            {
                db.GetCollection<OperationModel>("Operations").Delete(Query.All());
            }
        }

        /// <summary>
        /// Updates the operation.
        /// </summary>
        /// <param name="operationID">The operation identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="price">The price.</param>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="recurrent">if set to <c>true</c> [recurrent].</param>
        /// <param name="date">The date.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool updateOperation(int operationID, string title, double price, int categoryId, OperationType type,
            bool recurrent, string date)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    OperationModel operation = db.GetCollection<OperationModel>("Operations").FindById(operationID);
                    CategoryModel category = db.GetCollection<CategoryModel>("Categories").FindById(categoryId);
                    AccountModel account =
                        db.GetCollection<AccountModel>("Accounts").FindById(operation.ForAccount.AccountModelId);
                    account = deleteFromBalance(operationID, db);
                    operation.Title = title;
                    operation.Category = category;
                    operation.OperationType = type;
                    operation.Recurrent = recurrent;
                    operation.OperationDate = Convert.ToDateTime(date);
                    operation.Price = price;
                    operation.isTreated = false;
                    updateBalance(account, operation, db);
                    return db.GetCollection<OperationModel>("Operations").Update(operation);
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteInfo($"Error updating operation : {e.Message}");
                return false;
            }
        }

        /// <summary>
        /// Deletes the operation.
        /// </summary>
        /// <param name="operationID">The operation identifier.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool deleteOperation(int operationID)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    AccountModel account = deleteFromBalance(operationID, db);
                    updateBalanceStateObject(account);
                    return db.GetCollection<OperationModel>("Operations").Delete(operationID);
                }
            }
            catch (Exception exception)
            {
                PackageHost.WriteInfo($"Error deleting operation : {exception.Message}");
                return false;
            }
        }

        /// <summary>
        /// Deletes from balance.
        /// </summary>
        /// <param name="operationID">The operation identifier.</param>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public AccountModel deleteFromBalance(int operationID, LiteDatabase db)
        {
            try
            {
                OperationModel operation = db.GetCollection<OperationModel>("Operations").FindById(operationID);
                AccountModel account =
                    db.GetCollection<AccountModel>("Accounts").FindById(operation.ForAccount.AccountModelId);
                if (operation.isTreated)
                {
                    account.Balance = computeDebitOrCredit(operation, account.Balance, true);
                    db.GetCollection<AccountModel>("Accounts").Update(account);

                    return account;
                }
                return account;
            }
            catch (Exception exception)
            {
                PackageHost.WriteInfo($"Error deleting operation from balance {exception.Message}");
                return null;
            }
        }

        #endregion

        #region Info updaters

        /// <summary>
        /// Updates all balances.
        /// </summary>
        [MessageCallback]
        public void updateAllBalances()
        {
            PackageHost.WriteInfo("Launching balances update at {0}", DateTime.Now);
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    foreach (AccountModel account in db.GetCollection<AccountModel>("Accounts").FindAll())
                    {
                        var operations = db.GetCollection<OperationModel>("Operations")
                            .FindAll()
                            .Where(
                                model =>
                                    model.isTreated == false && model.OperationDate.Date <= DateTime.Now.Date &&
                                    model.ForAccount.AccountModelId == account.AccountModelId);

                        if (operations.Count() != 0)
                        {
                            foreach (OperationModel operation in operations)
                            {
                                updateBalance(account, operation, db);
                            }
                            //updateBalanceStateObject(account.AccountModelId, db);
                        }
                        else
                        {
                            PackageHost.WriteInfo("No operations to update");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while updating balances : {e.Message}");
            }
        }

        /// <summary>
        /// Updates the balance.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="operation">The operation.</param>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public bool updateBalance(AccountModel account, OperationModel operation, LiteDatabase db)
        {
            try
            {
                if (operation.OperationDate.Date <= DateTime.Now.Date && !operation.isTreated)
                {
                    PackageHost.WriteInfo(
                        $"Operation {operation.Title} will be updated on account {account.AccountName}");
                    account.Balance = computeDebitOrCredit(operation, account.Balance, false);
                    operation.isTreated = true;
                    db.GetCollection<OperationModel>("Operations").Update(operation);
                    db.GetCollection<AccountModel>("Accounts").Update(account);
                    if (operation.Recurrent)
                    {
                        //Si l'opération récurrente passe à traitée pour ce mois ci alors on l'ajoute pour le mois prochain
                        OperationModel operationRecurNextMonth = new OperationModel(operation.Executant, operation.Title,
                            operation.Price, true, operation.Category, operation.ForAccount,
                            operation.OperationDate.AddMonths(1), operation.OperationType);
                        db.GetCollection<OperationModel>("Operations").Insert(operationRecurNextMonth);
                    }
                    updateBalanceStateObject(account, operation);
                    return true;
                }
                updateBalanceStateObject(account, operation);
                return false;
            }
            catch (Exception exception)
            {
                PackageHost.WriteInfo($"Error while updating balance : {exception.Message}");
                return false;
            }
        }

        /// <summary>
        /// Updates the balance state object.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="operation">The operation.</param>
        public void updateBalanceStateObject(AccountModel account, OperationModel operation = null)
        {
            try
            {
                if (operation != null)
                {
                    string lastOperationType = getLastOperationType(operation);
                    PackageHost.PushStateObject("Account." + account.AccountModelId, new AccountStateObject()
                    {
                        ID = account.AccountModelId,
                        Name = account.AccountName,
                        Balance = account.Balance
                    },
                    metadatas: new Dictionary<string, object> { {"lastOperationType", lastOperationType} } );
                }
                else
                {
                    PackageHost.PushStateObject("Account." + account.AccountModelId, new AccountStateObject()
                    {
                        ID = account.AccountModelId,
                        Name = account.AccountName,
                        Balance = account.Balance
                    },
                    metadatas: new Dictionary<string, object> { { "lastOperationType", "deleteOperation" } });
                }
                
            }
            catch (Exception e)
            {
                PackageHost.WriteInfo($"Error while pushing state Object account {e.Message}");
            }
        }

        #endregion

        #region Compute Tools

        /// <summary>
        /// Computes the debit or credit.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="number">The number.</param>
        /// <param name="opposite">if set to <c>true</c> [opposite].</param>
        /// <returns></returns>
        public double computeDebitOrCredit(OperationModel operation, double number, bool opposite)
        {
            if (opposite)
            {
                if (operation.OperationType == OperationType.DEBIT)
                {
                    number += operation.Price;
                }
                else
                {
                    number -= operation.Price;
                }
                return number;
            }
            else
            {
                if (operation.OperationType == OperationType.DEBIT)
                {
                    number -= operation.Price;
                }
                else
                {
                    number += operation.Price;
                }
                return number;
            }
        }

        #endregion

        #region Pushbullet Notification

        /// <summary>
        /// Sends the notification debit or credit.
        /// </summary>
        /// <param name="personId">The person identifier.</param>
        /// <param name="operation">The operation.</param>
        public void sendNotificationDebitOrCredit(int personId, OperationModel operation)
        {
            try
            {
                UserSettingModel settings = getSettingsForUser(personId);

                string notifMessage =
                    $"Une opération : {operation.Title} de {operation.Price} Euros a été passée sur le compte : {operation.ForAccount.AccountName} à la date du {operation.OperationDate.ToShortDateString()}";

                if ((bool) settings.NotificationSettings["AllOperations"])
                {
                    sendMessageToPushBullet("ConstellaBank", notifMessage);
                }
                else
                {
                    if ((bool) settings.NotificationSettings["OnlyDebitOperations"])
                    {
                        if ((long) settings.NotificationSettings["DebitCeiling"] > 0 &&
                            operation.Price >= (long) settings.NotificationSettings["DebitCeiling"])
                        {
                            sendMessageToPushBullet("ConstellaBank", notifMessage);
                        }
                        else
                        {
                            sendMessageToPushBullet("ConstellaBank", notifMessage);
                        }
                    }
                    else if ((bool) settings.NotificationSettings["OnlyCreditOperations"])
                    {
                        if ((long) settings.NotificationSettings["CreditCeiling"] > 0 &&
                            operation.Price >= (long) settings.NotificationSettings["CreditCeiling"])
                        {
                            sendMessageToPushBullet("ConstellaBank", notifMessage);
                        }
                        else
                        {
                            sendMessageToPushBullet("ConstellaBank", notifMessage);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while sending notification : {e.Message}");
            }
        }

        /// <summary>
        /// Sends the notification for account ceiling.
        /// </summary>
        /// <param name="personId">The person identifier.</param>
        /// <param name="account">The account.</param>
        public void sendNotificationAccountCeiling(int personId, AccountModel account)
        {
            try
            {
                UserSettingModel settings = getSettingsForUser(personId);
                if (settings.NotificationSettings.ContainsKey("AccountCeiling")  && account.Balance <= Convert.ToDouble(settings.NotificationSettings["AccountCeiling"]))
                {
                    string notifMessage = $"Le solde de votre compte : {account.AccountName} a atteint la valeur de {account.Balance} €";
                    sendMessageToPushBullet("ConstellaBank", notifMessage);
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while sending notification for account ceiling : {e.Message}");
            }
        }

        /// <summary>
        /// Sends the message to push bullet.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="body">The body.</param>
        public void sendMessageToPushBullet(string title, string body)
        {
            //Send message with standard method
            PackageHost.SendMessage(MessageScope.Create("PushBullet"), "PushNote",
                new object[] {title, body, "Device", null});
        }

        #endregion

        #region ESP Notifications

        /// <summary>
        /// Gets the type of the operation.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public string getLastOperationType(OperationModel operation)
        {
            if (operation.OperationDate <= DateTime.Now)
            {
                if (operation.OperationType == OperationType.DEBIT)
                {
                    return "ImmediatDebit";
                }
                else
                {
                    return "ImmediatCredit";
                }
            }
            else
            {
                if (operation.OperationType == OperationType.DEBIT)
                {
                    return "FutureDebit";
                }
                else
                {
                    return "FutureCredit";
                }
            }
        }

        #endregion

        #region Login Functions

        /// <summary>
        /// Creates the new user.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="loginName">Name of the login.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool createNewUser(string name, string loginName, string password)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    var users = db.GetCollection<PersonModel>("Users");
                    var settingsNewPerson = new UserSettingModel()
                    {
                        NotificationSettings = DefaultNotificationDitionnary
                    };
                    PersonModel person = new PersonModel() {LoginName = loginName, Password = password, Name = name, UserSettings = settingsNewPerson };
                    if (users.FindAll().Any(z => z.LoginName == loginName))
                    {
                        PackageHost.WriteInfo("User already exists");
                        return false;
                    }
                    db.GetCollection<UserSettingModel>("Settings").Insert(settingsNewPerson);
                    db.GetCollection<PersonModel>("Users").Insert(person);
                    AccountModel account = new AccountModel(0, $"Compte {person.Name}");
                    account.Users.Add(person);
                    db.GetCollection<AccountModel>("Accounts").Insert(account);
                    return true;
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteInfo($"Error while creating new user : {e.Message}");
                return false;
            }
        }

        /// <summary>
        /// Logins the user.
        /// </summary>
        /// <param name="loginName">Name of the login.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        [MessageCallback]
        public int loginUser(string loginName, string password)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    PersonModel person =
                        db.GetCollection<PersonModel>("Users")
                            .Find(z => z.LoginName == loginName && z.Password == password)
                            .FirstOrDefault();
                    if (person != null)
                    {
                        var account = db
                            .GetCollection<AccountModel>("Accounts")
                            .Include(z => z.Users)
                            .FindAll()
                            .FirstOrDefault(z => z.Users.Exists(model => model.PersonModelId == person.PersonModelId));
                        if (account != null)
                        {
                            LoginStateObject = new { ID = account.AccountModelId, Account = $"Account-{account.AccountModelId}", BalanceOnLogin = account.Balance };
                            //PackageHost.RegisterStateObjectLinks(LoginStateObject);
                            PackageHost.PushStateObject("CurrentConnection", LoginStateObject);
                            return account.AccountModelId;
                        }
                        return -1;
                    }
                    return -1;
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while logging user : {e.Message}");
                return -1;
            }
        }

        #endregion

        #region User configuration

        /// <summary>
        /// Changes the user password.
        /// </summary>
        /// <param name="accountID">The account identifier.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool changeUserPassword(int accountID, string password)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    PersonModel person = db.GetCollection<PersonModel>("Users").FindById(accountID);
                    person.Password = password;
                    return db.GetCollection<PersonModel>("Users").Update(person);
                }
            }
            catch (Exception exception)
            {
                PackageHost.WriteError($"Error while changing password : {exception.Message}");
                return false;
            }
        }

        /// <summary>
        /// Changes the name of the user.
        /// </summary>
        /// <param name="accountID">The account identifier.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool changeUserName(int accountID, string name)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    PersonModel person = db.GetCollection<PersonModel>("Users").FindById(accountID);
                    person.Name = name;
                    return db.GetCollection<PersonModel>("Users").Update(person);
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while changing user name : {e.Message}");
                return false;
            }
        }

        /// <summary>
        /// Changes the name of the account.
        /// </summary>
        /// <param name="accountNumber">The account number.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool changeAccountName(int accountNumber, string name)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    AccountModel account = db.GetCollection<AccountModel>("Accounts").FindById(accountNumber);
                    account.AccountName = name;
                    return db.GetCollection<AccountModel>("Accounts").Update(account);
                }
            }
            catch (Exception exception)
            {
                PackageHost.WriteError($"Error while changing bank account name : {exception.Message}");
                return false;
            }
        }

        #endregion

        #region User Settings

        /// <summary>
        /// Gets the settings for user.
        /// </summary>
        /// <param name="accountID">The account identifier.</param>
        /// <returns></returns>
        [MessageCallback]
        public UserSettingModel getSettingsForUser(int accountID)
        {
            try
            {
                using (var db = new LiteDatabase(urlDatabase))
                {
                    PersonModel person = db.GetCollection<PersonModel>("Users")
                        .Include(model => model.UserSettings)
                        .FindById(accountID);
                    return person.UserSettings;
                }
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while getting user settings : {e.Message}");
                return null;
            }
        }

        /// <summary>
        /// Updates the user notification settings.
        /// </summary>
        /// <param name="personId">The person identifier.</param>
        /// <param name="notificationSettings">The notification settings.</param>
        /// <returns></returns>
        [MessageCallback]
        public bool updateUserNotificationSettings(int personId, Dictionary<string, object> notificationSettings)
        {
            try
            {
                //Solution évolutive on modifie les valeurs des clés existantes dans le dictionnaire
                //Si une clé n'existe pas alors elle est créée, ce qui permettra d'ajouter des settings par la suite
                using (LiteDatabase db = new LiteDatabase(urlDatabase))
                {
                    UserSettingModel settings = db.GetCollection<UserSettingModel>("Settings")
                        .FindOne(
                            model =>
                                model.UserSettingModelID ==
                                db.GetCollection<PersonModel>("Users").FindById(personId).PersonModelId);

                    foreach (KeyValuePair<string, object> notificationSetting in notificationSettings)
                    {
                        if (settings.NotificationSettings.ContainsKey(notificationSetting.Key))
                        {
                            if (notificationSetting.Value is string && notificationSetting.Value == "")
                            {
                                settings.NotificationSettings[notificationSetting.Key] = 0;
                            }
                            else
                            {
                                settings.NotificationSettings[notificationSetting.Key] = notificationSetting.Value;
                            }
                        }
                        else
                        {
                            settings.NotificationSettings.Add(notificationSetting.Key, notificationSetting.Value);
                        }
                    }
                    db.GetCollection<UserSettingModel>("Settings").Update(settings);
                }
                return true;
            }
            catch (Exception e)
            {
                PackageHost.WriteError($"Error while updating user notification settings : {e.Message}");
                return false;
            }
        }

        #endregion
    }
}